# Tirage au sort

Application permettant de sélectionner

- une matière
- une ou plusieurs classes
- les étudiants présents

puis d'effectuer un tirage au sort sans remise des étudiants présents