﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TirageAuSort.classes;
using MySql.Data.MySqlClient;

namespace TirageAuSort.dao
{
    public class EtudiantDao
    {
        readonly static MySqlConnection connection = Dal.GetConnection();
        public static List<Etudiant> GetAllBy(Classe classe)
        {
            List<Etudiant> list = new List<Etudiant>();

            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "" +
                "SELECT * " +
                "FROM `etudiant` " +
                "WHERE `id_classe` = ?id;";

            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id", classe.Id));
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Etudiant item = new Etudiant()
                {
                    Id = (int)reader["id"],
                    Nom = (string)reader["nom"],
                    Prenom = (string)reader["prenom"],
                    Compteur = (int)reader["compteur"],
                    IdClasse = (int)reader["id_classe"]
                };
                list.Add(item);
            }
            reader.Close();
            connection.Close();

            return list;
        }

        public static void IncrementerCompteur(Etudiant etudiant)
        {
            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "" +
                "UPDATE `etudiant` " +
                "SET `compteur` = `compteur` + 1 " +
                "WHERE `id` = ?id;";

            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id", etudiant.Id));
            command.ExecuteNonQuery();

            etudiant.Compteur++;

            connection.Close();
        }
        public static void RaZCompteur(Etudiant etudiant)
        {
            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "" +
                "UPDATE `etudiant` " +
                "SET `compteur` = 0 " +
                "WHERE `id` = ?id;";

            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id", etudiant.Id));
            command.ExecuteNonQuery();

            etudiant.Compteur = 0;

            connection.Close();
        }
    }
}
