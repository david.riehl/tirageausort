﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TirageAuSort.classes;
using MySql.Data.MySqlClient;

namespace TirageAuSort.dao
{
    public static class ClasseDao
    {
        readonly static MySqlConnection connection = Dal.GetConnection();
        public static List<Classe> GetAllBy(Matiere matiere)
        {
            List<Classe> list = new List<Classe>();

            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "" +
                "SELECT * " +
                "FROM `classe` " +
                "INNER JOIN `affecter` ON `affecter`.`id_classe` = `classe`.`id` " +
                "WHERE `affecter`.`id_matiere` = ?id;";

            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id", matiere.Id));
            MySqlDataReader reader = command.ExecuteReader();

            while(reader.Read())
            {
                Classe item = new Classe() {
                    Id = (int)reader["id"],
                    Nom = (string)reader["nom"]
                };
                list.Add(item);
            }
            reader.Close();
            connection.Close();

            return list;
        }

        public static Classe Get(int id)
        {
            Classe item = null;

            connection.Open();

            MySqlCommand command = connection.CreateCommand();

            command.CommandText = "" +
                "SELECT * " +
                "FROM `classe` " +
                "WHERE `id` = ?id;";

            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id", id));
            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                item = new Classe()
                {
                    Id = (int)reader["id"],
                    Nom = (string)reader["nom"]
                };
            }
            reader.Close();
            connection.Close();

            return item;
        }
    }
}
