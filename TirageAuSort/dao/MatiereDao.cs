﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TirageAuSort.classes;
using MySql.Data.MySqlClient;

namespace TirageAuSort.dao
{
    public static class MatiereDao
    {
        readonly static MySqlConnection connection = Dal.GetConnection();
        public static List<Matiere> GetAll()
        {
            List<Matiere> list = new List<Matiere>();

            connection.Open();

            MySqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM `matiere`;";
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Matiere item = new Matiere()
                {
                    Id = (int)reader["id"],
                    Nom = (string)reader["nom"]
                };
                list.Add(item);
            }
            reader.Close();
            connection.Close();

            return list;
        }
    }
}
