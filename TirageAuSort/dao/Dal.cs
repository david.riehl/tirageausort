﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace TirageAuSort.dao
{
    public static class Dal
    {
        private static MySqlConnection connection;
        public static MySqlConnection GetConnection()
        {
            if (connection == null)
            {
                MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder
                {
                    Server = ConfigurationManager.AppSettings.Get("Server"),
                    Port = Convert.ToUInt32(ConfigurationManager.AppSettings.Get("Port")),
                    UserID = ConfigurationManager.AppSettings.Get("UserID"),
                    Password = ConfigurationManager.AppSettings.Get("Password"),
                    Database = ConfigurationManager.AppSettings.Get("Database"),
                    CharacterSet = ConfigurationManager.AppSettings.Get("CharacterSet")
                };

                connection = new MySqlConnection(builder.ToString());
            }
            return connection;
        }
    }
}
