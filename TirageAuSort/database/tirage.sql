-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 10 oct. 2019 à 18:06
-- Version du serveur :  10.4.6-MariaDB
-- Version de PHP :  7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tirage`
--

-- --------------------------------------------------------

--
-- Structure de la table `affecter`
--

CREATE TABLE `affecter` (
  `id_matiere` int(11) NOT NULL,
  `id_classe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `affecter`
--

INSERT INTO `affecter` (`id_matiere`, `id_classe`) VALUES
(3, 5),
(3, 6),
(3, 7),
(15, 2),
(15, 4),
(16, 2),
(16, 4),
(17, 2),
(17, 4),
(20, 2),
(20, 4),
(21, 2),
(21, 4);

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

CREATE TABLE `classe` (
  `id` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`id`, `nom`) VALUES
(1, 'SIO 2 (SISR)'),
(2, 'SIO 2 (SLAM)'),
(3, 'UFA 2 SIO (SISR)'),
(4, 'UFA 2 SIO (SLAM)'),
(5, 'SIO 1 (G1)'),
(6, 'SIO 1 (G2)'),
(7, 'GRETA');

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `compteur` int(11) NOT NULL DEFAULT 0,
  `id_classe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`id`, `nom`, `prenom`, `compteur`, `id_classe`) VALUES
(1, 'HOURDIN', 'Florian', 1, 2),
(2, 'DIERICKX', 'Nathan', 1, 2),
(3, 'BIENKOWSKI', 'Rudy', 0, 2),
(4, 'LEPILLEZ', 'Thomas', 0, 2),
(5, 'DHOLLANDE', 'Thomas', 0, 2),
(6, 'FELDER', 'Maxime', 1, 2),
(7, 'DEMARCQ', 'Alexandre', 1, 2),
(8, 'DUMEZ', 'Rodrigue', 1, 2),
(9, 'LEGER', 'Alexis', 1, 2),
(10, 'BIENVENU', 'Jovan', 1, 4),
(11, 'BRUNNET', 'Guillaume', 1, 4),
(12, 'FAVOREL', 'Jonathan', 1, 4),
(13, 'Bocquet', 'Gary', 0, 5),
(14, 'Claux-Andrieux', 'Estelle', 0, 5),
(15, 'Coulon', 'Axel', 0, 5),
(16, 'Denaes', 'Léo', 0, 5),
(17, 'Erken', 'Hasan', 0, 5),
(18, 'Gallego-Garcia', 'Valentin', 0, 5),
(19, 'Lefebvre', 'Théo', 0, 5),
(20, 'Lemaire', 'Clément', 0, 5),
(21, 'Lys', 'Thomas', 0, 5),
(22, 'M’Voulana', 'Nahimou', 0, 5),
(23, 'Ourdouillie', 'Benjamin', 0, 5),
(24, 'Petit', 'Romain', 0, 5),
(25, 'Renaudin', 'Dorian', 0, 5),
(26, 'Schmidt', 'Marine', 0, 5),
(27, 'Sital', 'Mathéo', 0, 5),
(28, 'Strugala', 'Erwan', 0, 5),
(29, 'Barka', 'Assim', 0, 6),
(30, 'Beulque', 'Théo', 0, 6),
(31, 'Braem', 'Pierre', 0, 6),
(32, 'Dauchelle', 'Antoine', 0, 6),
(33, 'Delhaye', 'Florian', 0, 6),
(34, 'Gossuin', 'Thomas', 0, 6),
(35, 'Heurtevent', 'Clément', 0, 6),
(36, 'Hot', 'Matthieu', 0, 6),
(37, 'Lepine', 'Anthony', 0, 6),
(38, 'Lesnes', 'Alexis', 0, 6),
(39, 'Lienart', 'Thomas', 0, 6),
(40, 'Marsac', 'Axel', 0, 6),
(41, 'Nesterenko', 'Justine', 0, 6),
(42, 'Pivion', 'Thibault', 0, 6),
(43, 'Proust', 'Kevin', 0, 6),
(44, 'Reumont', 'Théo', 0, 6),
(45, 'Robert', 'Corentin', 0, 6),
(46, 'Sanpeur', 'Jordan', 0, 6),
(47, 'Sarteel', 'Laurie', 0, 6),
(48, 'FERRARO', 'Romuald', 0, 7);

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

CREATE TABLE `matiere` (
  `id` int(11) NOT NULL,
  `nom` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`id`, `nom`) VALUES
(1, 'SI 1'),
(2, 'SI 2'),
(3, 'SI 3'),
(4, 'SI 4'),
(5, 'SI 5'),
(6, 'SI 6'),
(7, 'SI 7'),
(8, 'SISR 1'),
(9, 'SISR 2'),
(10, 'SISR 3'),
(11, 'SISR 4'),
(12, 'SISR 5'),
(13, 'SLAM 1'),
(14, 'SLAM 2'),
(15, 'SLAM 3'),
(16, 'SLAM 4'),
(17, 'SLAM 5'),
(18, 'PPE 1'),
(19, 'PPE 2'),
(20, 'PPE 3'),
(21, 'PPE 4');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `affecter`
--
ALTER TABLE `affecter`
  ADD PRIMARY KEY (`id_matiere`,`id_classe`),
  ADD KEY `id_classe` (`id_classe`);

--
-- Index pour la table `classe`
--
ALTER TABLE `classe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_classe` (`id_classe`);

--
-- Index pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `classe`
--
ALTER TABLE `classe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT pour la table `matiere`
--
ALTER TABLE `matiere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `affecter`
--
ALTER TABLE `affecter`
  ADD CONSTRAINT `affecter_ibfk_1` FOREIGN KEY (`id_classe`) REFERENCES `classe` (`id`),
  ADD CONSTRAINT `affecter_ibfk_2` FOREIGN KEY (`id_matiere`) REFERENCES `matiere` (`id`);

--
-- Contraintes pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD CONSTRAINT `etudiant_ibfk_1` FOREIGN KEY (`id_classe`) REFERENCES `classe` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
