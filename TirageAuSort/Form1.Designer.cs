﻿namespace TirageAuSort
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSuivant = new System.Windows.Forms.Button();
            this.lstDispo = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lstPassés = new System.Windows.Forms.ListBox();
            this.cmbMatiere = new System.Windows.Forms.ComboBox();
            this.chklstClasses = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chklstEtudiants = new System.Windows.Forms.CheckedListBox();
            this.grpSelection = new System.Windows.Forms.GroupBox();
            this.grpTirage = new System.Windows.Forms.GroupBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.grpSelection.SuspendLayout();
            this.grpTirage.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSuivant
            // 
            this.btnSuivant.Location = new System.Drawing.Point(135, 143);
            this.btnSuivant.Name = "btnSuivant";
            this.btnSuivant.Size = new System.Drawing.Size(75, 23);
            this.btnSuivant.TabIndex = 0;
            this.btnSuivant.Text = "Au Suivant !";
            this.btnSuivant.UseVisualStyleBackColor = true;
            this.btnSuivant.Click += new System.EventHandler(this.BtnSuivant_Click);
            // 
            // lstDispo
            // 
            this.lstDispo.FormattingEnabled = true;
            this.lstDispo.Location = new System.Drawing.Point(9, 32);
            this.lstDispo.Name = "lstDispo";
            this.lstDispo.Size = new System.Drawing.Size(120, 238);
            this.lstDispo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Etudiants en attente";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(213, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Etudiants déjà passés";
            // 
            // lstPassés
            // 
            this.lstPassés.FormattingEnabled = true;
            this.lstPassés.Location = new System.Drawing.Point(216, 32);
            this.lstPassés.Name = "lstPassés";
            this.lstPassés.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lstPassés.Size = new System.Drawing.Size(120, 238);
            this.lstPassés.TabIndex = 4;
            // 
            // cmbMatiere
            // 
            this.cmbMatiere.FormattingEnabled = true;
            this.cmbMatiere.Location = new System.Drawing.Point(6, 32);
            this.cmbMatiere.Name = "cmbMatiere";
            this.cmbMatiere.Size = new System.Drawing.Size(121, 21);
            this.cmbMatiere.TabIndex = 6;
            this.cmbMatiere.SelectedIndexChanged += new System.EventHandler(this.CmbMatiere_SelectedIndexChanged);
            // 
            // chklstClasses
            // 
            this.chklstClasses.Enabled = false;
            this.chklstClasses.FormattingEnabled = true;
            this.chklstClasses.Location = new System.Drawing.Point(6, 72);
            this.chklstClasses.Name = "chklstClasses";
            this.chklstClasses.Size = new System.Drawing.Size(120, 94);
            this.chklstClasses.TabIndex = 7;
            this.chklstClasses.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ChklstClasses_ItemCheck);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Matière";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Classes";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(133, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Etudiants Présents";
            // 
            // chklstEtudiants
            // 
            this.chklstEtudiants.Enabled = false;
            this.chklstEtudiants.FormattingEnabled = true;
            this.chklstEtudiants.Location = new System.Drawing.Point(130, 32);
            this.chklstEtudiants.Name = "chklstEtudiants";
            this.chklstEtudiants.Size = new System.Drawing.Size(120, 244);
            this.chklstEtudiants.TabIndex = 11;
            this.chklstEtudiants.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ChklstEtudiants_ItemCheck);
            // 
            // grpSelection
            // 
            this.grpSelection.Controls.Add(this.chklstEtudiants);
            this.grpSelection.Controls.Add(this.cmbMatiere);
            this.grpSelection.Controls.Add(this.label5);
            this.grpSelection.Controls.Add(this.chklstClasses);
            this.grpSelection.Controls.Add(this.label4);
            this.grpSelection.Controls.Add(this.label3);
            this.grpSelection.Location = new System.Drawing.Point(12, 12);
            this.grpSelection.Name = "grpSelection";
            this.grpSelection.Size = new System.Drawing.Size(260, 290);
            this.grpSelection.TabIndex = 12;
            this.grpSelection.TabStop = false;
            this.grpSelection.Text = "Sélection";
            // 
            // grpTirage
            // 
            this.grpTirage.Controls.Add(this.btnReset);
            this.grpTirage.Controls.Add(this.label1);
            this.grpTirage.Controls.Add(this.lstDispo);
            this.grpTirage.Controls.Add(this.lstPassés);
            this.grpTirage.Controls.Add(this.label2);
            this.grpTirage.Controls.Add(this.btnSuivant);
            this.grpTirage.Location = new System.Drawing.Point(278, 12);
            this.grpTirage.Name = "grpTirage";
            this.grpTirage.Size = new System.Drawing.Size(349, 290);
            this.grpTirage.TabIndex = 13;
            this.grpTirage.TabStop = false;
            this.grpTirage.Text = "Tirage au sort";
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(135, 247);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 5;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // Form1
            // 
            this.AcceptButton = this.btnSuivant;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 311);
            this.Controls.Add(this.grpTirage);
            this.Controls.Add(this.grpSelection);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Au tableau !";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpSelection.ResumeLayout(false);
            this.grpSelection.PerformLayout();
            this.grpTirage.ResumeLayout(false);
            this.grpTirage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSuivant;
        private System.Windows.Forms.ListBox lstDispo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lstPassés;
        private System.Windows.Forms.ComboBox cmbMatiere;
        private System.Windows.Forms.CheckedListBox chklstClasses;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckedListBox chklstEtudiants;
        private System.Windows.Forms.GroupBox grpSelection;
        private System.Windows.Forms.GroupBox grpTirage;
        private System.Windows.Forms.Button btnReset;
    }
}

