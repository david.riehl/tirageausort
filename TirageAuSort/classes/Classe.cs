﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TirageAuSort.dao;

namespace TirageAuSort.classes
{
    public class Classe
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        private List<Etudiant> etudiants;
        public List<Etudiant> Etudiants
        {
            get
            {
                if (etudiants == null) { etudiants = Etudiant.GetAllBy(this); }
                return etudiants;
            }
        }
        public List<Etudiant> EtudiantsDispos
        {
            get
            {
                if (etudiants == null) { etudiants = Etudiant.GetAllBy(this); }
                return etudiants.FindAll(e => e.Compteur == 0).ToList();
            }
        }
        public List<Etudiant> EtudiantsNonDispos
        {
            get
            {
                if (etudiants == null) { etudiants = Etudiant.GetAllBy(this); }
                return etudiants.FindAll(e => e.Compteur != 0).ToList();
            }
        }
        public static Classe Get(int id) { return ClasseDao.Get(id); }
        public static List<Classe> GetAllBy(Matiere matiere) { return ClasseDao.GetAllBy(matiere); }
    }
}
