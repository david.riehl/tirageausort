﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TirageAuSort.dao;

namespace TirageAuSort.classes
{
    public class Matiere
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        private List<Classe> classes;
        public List<Classe> Classes
        {
            get
            {
                if (classes == null) { classes = Classe.GetAllBy(this); }
                return classes;
            }
        }

        public static List<Matiere> GetAll() { return MatiereDao.GetAll(); }
    }
}
