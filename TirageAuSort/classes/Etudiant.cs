﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TirageAuSort.dao;

namespace TirageAuSort.classes
{
    public class Etudiant : IComparable
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string NomComplet { get { return Prenom + " " + Nom; } }
        public string NomCompletCompteur { get { return Prenom + " " + Nom + " (" + Compteur + ")"; } }
        public int Compteur { get; set; }
        public int IdClasse { get; set; }
        private Classe classe;
        public Classe Classe
        {
            get
            {
                if (classe == null) { classe = Classe.Get(IdClasse); }
                return classe;
            }
        }

        public static List<Etudiant> GetAllBy(Classe classe) { return EtudiantDao.GetAllBy(classe); }

        public static void IncrementerCompteur(Etudiant etudiant)
        {
            EtudiantDao.IncrementerCompteur(etudiant);
        }

        public static void RaZAllCompteur(List<Etudiant> etudiants)
        {
            foreach(Etudiant etudiant in etudiants)
            {
                EtudiantDao.RaZCompteur(etudiant);
            }
        }
        public static void RaZCompteur(Etudiant etudiant) { EtudiantDao.RaZCompteur(etudiant); }

        public int CompareTo(Etudiant etudiant)
        {
            string me = this.Nom + "," + this.Prenom;
            string other = etudiant.Nom + "," + etudiant.Prenom;
            return me.CompareTo(other);
        }
        public int CompareTo(object obj)
        {
            int res;
            if (obj is Etudiant etudiant)
            {
                res = this.CompareTo(etudiant);
            }
            else
            {
                res = this.CompareTo("");
            }
            return res;
        }
    }
}
