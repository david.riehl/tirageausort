﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TirageAuSort.classes;

namespace TirageAuSort
{
    public partial class Form1 : Form
    {
        bool loadingEtudiant = false;
        Random random;

        List<Etudiant> dispo;
        readonly BindingSource bsDispo = new BindingSource();

        List<Etudiant> passés;
        readonly BindingSource bsPassés = new BindingSource();

        List<Matiere> matieres;
        readonly BindingSource bsMatieres = new BindingSource();

        readonly BindingSource bsClasses = new BindingSource();

        List<Etudiant> etudiants;
        readonly BindingSource bsEtudiants = new BindingSource();

        Etudiant last;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitRandom();
            InitMatiere();
            InitClasse();
            InitEtudiant();
            InitDispo();
            InitPasse();
        }

        private void InitRandom()
        {
            int seed = (int)(DateTime.Now.ToBinary() % int.MaxValue);
            random = new Random(seed);
        }

        private void InitMatiere()
        {
            matieres = Matiere.GetAll();
            bsMatieres.DataSource = matieres;
            cmbMatiere.DataSource = bsMatieres;
            cmbMatiere.DisplayMember = "Nom";
        }

        private void InitClasse()
        {
            ((ListBox)chklstClasses).DataSource = bsClasses;
            ((ListBox)chklstClasses).DisplayMember = "Nom";
        }

        private void InitEtudiant()
        {
            etudiants = new List<Etudiant>();
            bsEtudiants.DataSource = etudiants;
            ((ListBox)chklstEtudiants).DataSource = bsEtudiants;
            ((ListBox)chklstEtudiants).DisplayMember = "NomComplet";
        }

        private void InitDispo()
        {
            dispo = new List<Etudiant>();
            bsDispo.DataSource = dispo;
            bsDispo.Sort = "Nom, Prenom";
            lstDispo.DataSource = bsDispo;
            lstDispo.DisplayMember = "NomCompletCompteur";
        }

        private void InitPasse()
        {
            passés = new List<Etudiant>();
            bsPassés.DataSource = passés;
            lstPassés.DataSource = bsPassés;
            lstPassés.DisplayMember = "NomCompletCompteur";
        }

        private void CmbMatiere_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbMatiere.SelectedIndex != -1)
            {
                List<Classe> classes = chklstClasses.CheckedItems.OfType<Classe>().ToList();
                foreach (Classe classe in classes)
                {
                    int index = chklstClasses.Items.IndexOf(classe);
                    chklstClasses.SetItemChecked(index, false);
                }

                Matiere matiere = (Matiere)cmbMatiere.SelectedItem;
                classes = matiere.Classes;
                bsClasses.DataSource = classes;
                bsClasses.ResetBindings(false);
                chklstClasses.Enabled = classes.Count > 0;
            }
        }

        private void ChklstClasses_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            loadingEtudiant = true;
            Classe classe = (Classe)chklstClasses.SelectedItem;
            if (e.NewValue == CheckState.Checked)
            {
                if (classe.Etudiants.Count != 0)
                {
                    // saving check state
                    Dictionary<Etudiant, bool> etudiantsStates = new Dictionary<Etudiant, bool>();
                    for(int index = 0; index < etudiants.Count; index ++)
                    {
                        Etudiant etudiant = etudiants[index];
                        bool state = chklstEtudiants.GetItemChecked(index);
                        etudiantsStates.Add(etudiant, state);
                    }

                    // adding new items
                    etudiants.AddRange(classe.Etudiants);
                    etudiants.Sort();
                    bsEtudiants.ResetBindings(false);

                    // restoring checkState for old items
                    foreach(KeyValuePair<Etudiant, bool> pair in etudiantsStates)
                    {
                        Etudiant etudiant = pair.Key;
                        bool state = pair.Value;
                        int index = etudiants.IndexOf(etudiant);
                        chklstEtudiants.SetItemChecked(index, state);
                    }

                    // checking new items
                    foreach (Etudiant etudiant in classe.Etudiants)
                    {
                        int index = etudiants.IndexOf(etudiant);
                        chklstEtudiants.SetItemChecked(index, true);
                    }

                    dispo.AddRange(classe.EtudiantsDispos);
                    dispo.Sort();
                    bsDispo.ResetBindings(false);

                    passés.AddRange(classe.EtudiantsNonDispos);
                    bsPassés.ResetBindings(false);

                    chklstEtudiants.Enabled = true;
                }
            }
            else
            {
                if (classe.Etudiants.Count != 0)
                {
                    // saving checkState
                    Dictionary<Etudiant, bool> etudiantsStates = new Dictionary<Etudiant, bool>();
                    for (int index = 0; index < etudiants.Count; index++)
                    {
                        Etudiant etudiant = etudiants[index];
                        bool state = chklstEtudiants.GetItemChecked(index);
                        etudiantsStates.Add(etudiant, state);
                    }

                    // Removing items
                    etudiants.RemoveAll(classe.Etudiants.Contains);
                    bsEtudiants.ResetBindings(true);

                    // restoring checkState for items
                    foreach (KeyValuePair<Etudiant, bool> pair in etudiantsStates)
                    {
                        Etudiant etudiant = pair.Key;
                        bool state = pair.Value;
                        int index = etudiants.IndexOf(etudiant);
                        if (index != -1)
                        {
                            chklstEtudiants.SetItemChecked(index, state);
                        }
                    }

                    dispo.RemoveAll(classe.Etudiants.Contains);
                    bsDispo.ResetBindings(false);

                    passés.RemoveAll(classe.Etudiants.Contains);
                    bsPassés.ResetBindings(false);

                    chklstEtudiants.Enabled = etudiants.Count > 0;
                }
            }
            loadingEtudiant = false;
        }

        private void ChklstEtudiants_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (! loadingEtudiant)
            {
                Etudiant etudiant = (Etudiant)chklstEtudiants.SelectedItem;
                if (e.NewValue == CheckState.Checked)
                {
                    if (etudiant.Compteur == 0)
                    {
                        bsDispo.Add(etudiant);
                        dispo.Sort();
                        bsDispo.ResetBindings(false);
                    }
                    else
                    {
                        bsPassés.Add(etudiant);
                    }
                }
                else
                {
                    bsDispo.Remove(etudiant);
                    bsPassés.Remove(etudiant);
                }
            }
        }

        private void BtnSuivant_Click(object sender, EventArgs e)
        {
            int index = random.Next(dispo.Count);
            Etudiant etudiant = (Etudiant)bsDispo[index];

            if (etudiant == last)
            {
                index = (index + 1) % dispo.Count;
                etudiant = (Etudiant)bsDispo[index];
            }

            last = etudiant;

            Etudiant.IncrementerCompteur(etudiant);

            bsDispo.Remove(etudiant);
            bsPassés.Add(etudiant);


            if (dispo.Count == 0)
            {
                BtnReset_Click(sender, e);
            }

            MessageBox.Show("Suivant : " + etudiant.NomComplet);
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            Etudiant.RaZAllCompteur(passés);
            dispo.AddRange(passés);
            dispo.Sort();
            bsPassés.Clear();
            bsDispo.ResetBindings(false);
        }
    }
}
